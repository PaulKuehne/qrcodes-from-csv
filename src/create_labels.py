from reportlab.pdfbase import pdfmetrics
from reportlab.graphics.barcode import qr
from reportlab.graphics.shapes import Drawing
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF as render_pdf
from reportlab.platypus import Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfbase.ttfonts import TTFont
from typing import List
from reportlab.lib.units import mm, inch
import pandas as pd
from dataclasses import dataclass

mm = mm*101./100.  # better printed size
# (this is individual for each
# printer/printer type)

PAGE_SZ = A4
LABEL_SZ = (52.5*mm, 29.7*mm)

ROWS = 10
COLS = 4

QR_SZ = 20. * mm  # QR code size
QR_POS_PRCN = (0., 15.)  # QR code position on label [percent]

CODE_TEXT_POS_PRCN = (5., 5.)  # code text position on label [percent]
BOOK_NAME_POS = (45., 5.)  # book name position on label [percent]
PAGE_OFFS = (4. * mm, 0.*mm)  # offset applies to all labels

# this will move first row and first column away from page edge whiteout affecting other rows and cols
PRINTER_MARGINS = (0.*mm, 3*mm)
# PRINTER_MARGINS = (4.2 * mm, 4.2*mm)

BOOK_NAME_W = 22 * mm  # width of paragraph with book name (used for wrapping)

# font used for printing russian letters, you might not need it.
pdfmetrics.registerFont(TTFont('DejaVuSerif', 'DejaVuSerif.ttf'))


@dataclass
class BookDescription:
    name: str = "Book name"
    code: str = "20-00001"


@dataclass
class Position:
    x: float = 0.
    y: float = 0.


@dataclass
class Index2d:
    x: int = 0
    y: int = 0


@dataclass
class LabelOffset:
    pos: Position
    idx: Index2d


def get_qr_code_drawing(code_value, size=QR_SZ):
    qr_code = qr.QrCodeWidget(code_value)
    bounds = qr_code.getBounds()
    qr_width = bounds[2] - bounds[0]
    qr_height = bounds[3] - bounds[1]
    drawing = Drawing(width=size, height=size, transform=[
                      size/qr_width, 0, 0, size/qr_height, 0, 0])

    drawing.add(qr_code)

    return drawing


def get_label_offs(index: int):
    assert index < ROWS*COLS
    idx_y = index // COLS
    idx_x = index % COLS
    offs_x = float(idx_x) * LABEL_SZ[0]+PAGE_OFFS[0]
    offs_y = float(idx_y) * LABEL_SZ[1]+PAGE_OFFS[1]
    if int(idx_y) == 0:
        offs_y += PRINTER_MARGINS[1]

    if int(idx_x) == 0:
        offs_x += PRINTER_MARGINS[0]
    return LabelOffset(Position(offs_x, offs_y), Index2d(idx_x, idx_y))


def get_qr_code_position(index: int):
    offs = get_label_offs(index=index)
    x = 0.01*QR_POS_PRCN[0]*LABEL_SZ[0] + offs.pos.x
    y = 0.01*QR_POS_PRCN[1]*LABEL_SZ[1] + offs.pos.y
    return (x, y)


def get_code_text_position(index: int):
    offs = get_label_offs(index=index)
    x = 0.01*CODE_TEXT_POS_PRCN[0]*LABEL_SZ[0] + offs.pos.x
    y = 0.01*CODE_TEXT_POS_PRCN[1]*LABEL_SZ[1] + offs.pos.y
    return (x, y)


def get_book_name_position(index: int):
    offs = get_label_offs(index=index)
    x = 0.01*BOOK_NAME_POS[0]*LABEL_SZ[0] + offs.pos.x
    y = 0.01*BOOK_NAME_POS[1]*LABEL_SZ[1] + offs.pos.y
    return (x, y)


def trim_text(text: str, max_len: int):
    words = text.split(' ')
    w = []
    _len = 0
    for word in words:
        _len += len(word)
        if _len < max_len:
            w.append(word)
        else:
            break
    return " ".join(w)


def create_qr_codes(books: List[BookDescription], filename: str = "qr_codes.pdf", start_idx: int = 0):
    canv = canvas.Canvas(filename=filename, pagesize=PAGE_SZ)
    labels_per_page = ROWS*COLS

    # split list into batches per page
    books_batches = [books[i:i + labels_per_page]
                     for i in range(0, len(books), labels_per_page)]

    for books_batch in books_batches:
        for i, book in enumerate(books_batch):
            drawing = get_qr_code_drawing(code_value=book.code, size=QR_SZ)

            # add qr code
            qr_position = get_qr_code_position(index=i+start_idx)
            render_pdf.draw(drawing=drawing, canvas=canv,
                            x=qr_position[0], y=qr_position[1])

            # add encoded value as string
            code_text_position = get_code_text_position(index=i+start_idx)
            canv.drawString(
                x=code_text_position[0], y=code_text_position[1], text=book.code)

            # add book name
            book_name_position = get_book_name_position(index=i)
            style = getSampleStyleSheet()["Normal"]  # дефолтовые стили
            style.fontName = 'DejaVuSerif'
            style.fontSize = 5
            style.leading = 7
            book_name_paragraph = Paragraph(text=trim_text(
                book.name, max_len=80), style=style)
            book_name_paragraph.wrapOn(
                canv=canv, aW=BOOK_NAME_W, aH=LABEL_SZ[1]*0.9)
            book_name_paragraph.drawOn(
                canvas=canv, x=book_name_position[0], y=book_name_position[1], _sW=1)
        canv.showPage()

    canv.save()


if __name__ == "__main__":

    # Read the CSV file
    items = pd.read_csv("csv_in/items.csv")
    # print(items.columns)
    titles = items[' "Title"'].to_list()
    codes = items["Barcode"].to_list()
    books = [BookDescription(name=title, code=code)
             for title, code in zip(titles, codes)]
    create_qr_codes(books=books, start_idx=0, filename="pdf_out/labels.pdf")
