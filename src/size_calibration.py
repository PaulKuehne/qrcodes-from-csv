from reportlab.lib import colors
from reportlab.graphics.shapes import *
from reportlab.graphics.shapes import Drawing
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF as render_pdf
from reportlab.lib.units import mm


# Creates a test pdf page that may help you calibrate the printed size.
# Depending on your printer, you may not need this or you can use the printer settings instead.
# In my case there is an offset of 1 mm per 100 mm (printed size is smaller than expected).
# So I use this adjustment:
mm = mm*101./100.

PAGE_SZ = A4


def draw_rect(x, y, w, h, c):

    d = Drawing(*A4)
    d.add(Rect(x=x, y=y, width=w, height=h, fillColor=colors.white))
    render_pdf.draw(drawing=d, canvas=c,
                    x=0*mm, y=0*mm)


if __name__ == "__main__":
    page = canvas.Canvas(
        filename="pdf_out/size_calibration.pdf", pagesize=PAGE_SZ)
    draw_rect(x=0., y=0., w=A4[0], h=A4[1], c=page)
    draw_rect(x=5.*mm, y=5.*mm, w=A4[0]-10.*mm, h=A4[1]-10*mm, c=page)
    draw_rect(x=100.*mm, y=100.*mm, w=100.*mm, h=100*mm, c=page)
    page.save()
