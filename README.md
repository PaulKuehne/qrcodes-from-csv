# qrcodes-from-csv

Creating Labels for books with QR codes and book names from CSV.

## Project status

This project is meant for personal use and not pretending to be usable for others

# Setup

This should be done once

```bash
$ python3 -m venv .venv
$ . .venv/bin/activate 
$ pip install -r requirements.txt
```

## Usage

- Data for labels is taken from csv file (`csv_in/items.csv`)
- generated labels are stored in  `pdf_out/labels.pdf` 

To run generation script:

```bash
$ . .venv/bin/activate 
$ python src/create_labels.py 
```
